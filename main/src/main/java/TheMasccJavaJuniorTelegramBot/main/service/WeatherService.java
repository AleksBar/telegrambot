package TheMasccJavaJuniorTelegramBot.main.service;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

@Component
public class WeatherService {
    public static String getWeather(String city) {
        try {
            URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=1673a5fee14c1a3f81cb283c99a6eabe&lang=ru&units=metric");
            Scanner scanner = new Scanner((InputStream) url.getContent());
            StringBuilder result = new StringBuilder();

            while (scanner.hasNext()){
                result.append(scanner.nextLine());

            }

            JSONObject json = new JSONObject(result.toString());
            JSONObject mainData = json.getJSONObject("main");
            BigDecimal temperature = mainData.getBigDecimal("temp");

            return "Температура в " + city + ": " + temperature + "°C";
        } catch (IOException e) {
            return null;
        }

    }

}