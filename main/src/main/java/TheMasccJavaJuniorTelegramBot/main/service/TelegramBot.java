package TheMasccJavaJuniorTelegramBot.main.service;

import TheMasccJavaJuniorTelegramBot.main.config.CommandsStore;
import TheMasccJavaJuniorTelegramBot.main.config.TelegramBotConfig;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;

@Component
@AllArgsConstructor

public class TelegramBot extends TelegramLongPollingBot {

    private final TelegramBotConfig telegramBotConfig;

    private CommandsStore store;


    /**
     * @param update commands:
     *               /start
     *               /help
     *               /currency
     *               /weather
     */

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            Message message = update.getMessage();
            String msg = message.getText();
            long chatId = message.getChatId();

            switch (msg) {
                case "/start":
                    startWindowHandler(chatId, message);
                    break;
                case "/help":
                    helpWindowHandler(chatId);
                    break;
                case "/currency":
                    currencyWindowHandler(chatId);
                    break;
                case "/weather":
                    weatherWindowHandler(chatId);
                    break;
                default:
                    defaultHandler(chatId, msg);
            }
        }
    }

    @Override
    public String getBotUsername() {
        return telegramBotConfig.getBotName();
    }

    @Override
    public String getBotToken() {
        return telegramBotConfig.getBotToken();
    }

    public List<String> getWindows() {
        return List.of("start", "help", "currency", "weather");
    }

    public List<String> getDefaultCommands() {
        return List.of("/start", "/help", "/currency", "/weather");
    }

    private String getCommandsMessage() {
        StringBuilder commandsBuilder = new StringBuilder();
        getDefaultCommands().forEach(command -> commandsBuilder.append(command).append("\n"));

        return commandsBuilder.toString();
    }

    private String getCurrencyCommandsMessage() {
        StringBuilder commandsBuilder = new StringBuilder();

        commandsBuilder.append("Выберите валюту или введите нужную:\n");
        List.of("/EUR", "/USD", "/RUB").forEach(command -> commandsBuilder.append(command).append("\n"));
        commandsBuilder.append("\n").append("Вы можете ввести свою валюту в формате /USD\n /help - Помощь");

        return commandsBuilder.toString();
    }

    private String getWeatherCommandsMessage() {
        StringBuilder commandsBuilder = new StringBuilder();

        commandsBuilder.append("Выберите город или введите свой:\n");
        List.of("/Moscow", "/Ivanovo", "/Kirov").forEach(command -> commandsBuilder.append(command).append("\n"));
        commandsBuilder.append("\n").append("Вы можете ввести свой город в формате /Moscow\n Помощь - /help");

        return commandsBuilder.toString();
    }

    private void sendGreetingMessage(long chatId, String name) {
        String answer = "Привет, " + name + "!\n"
                + "Меня зовут " + telegramBotConfig.getBotName() + "\n"
                + "Я создан для обучения, вот мои доступные команды:\n";

        sendMessage(chatId, answer + getCommandsMessage());
    }

    private void startWindowHandler(long chatId, Message message) {
        store.setActiveState("start");
        sendGreetingMessage(chatId, message.getChat().getFirstName() + " " + message.getChat().getLastName());
    }

    private void helpWindowHandler(long chatId) {
        store.setActiveState("help");
        sendMessage(chatId, "Доступные команды:\n" + getCommandsMessage());
    }

    private void currencyWindowHandler(long chatId) {
        store.setActiveState("currency");
        sendMessage(chatId, getCurrencyCommandsMessage());
    }

    private void weatherWindowHandler(long chatId) {
        store.setActiveState("weather");
        sendMessage(chatId, getWeatherCommandsMessage());
    }

    private void defaultHandler(long chatId, String msg) {

        // показать валюту если активно окно валюты
        if (store.checkState("currency")) {
            if (msg.startsWith("/") && msg.length() == 4) {
                String currencyValue = msg.substring(1, 4).toUpperCase();
                String currency = CurrencyService.getCurrency(currencyValue);

                if (currency != null) {
                    sendMessage(chatId, currency + "\n Помощь - /help");
                } else {
                    sendMessage(chatId, "Не удалось получить курс");
                    currencyWindowHandler(chatId);
                }

            } else {
                sendMessage(chatId, "Некорректные данные");
                currencyWindowHandler(chatId);
            }

        }

        // показать погоду если активно окно погоды
        if (store.checkState("weather")) {
            if (msg.startsWith("/")) {
                String weather = WeatherService.getWeather(msg.substring(1));

                if (weather != null) {
                    sendMessage(chatId, weather + "\n Помощь - /help");
                } else {
                    sendMessage(chatId, "Ваш город не найден");
                    weatherWindowHandler(chatId);
                }
            } else {
                weatherWindowHandler(chatId);
            }

        }
        //дефолтное сообщение
        if (!store.checkState("weather") && !store.checkState("currency")) {
            helpWindowHandler(chatId);
        }
    }

    private void sendMessage(long chatId, String answer) {
        SendMessage sendMessage = new SendMessage();

        sendMessage.setChatId(chatId);
        sendMessage.setText(answer);

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            throw new IllegalArgumentException("Ошибка в " + chatId + ", сообщение: " + answer);
        }
    }

    public void setStore(CommandsStore store) {
        this.store = store;
    }
}
