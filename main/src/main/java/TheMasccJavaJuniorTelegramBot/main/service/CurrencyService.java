package TheMasccJavaJuniorTelegramBot.main.service;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

@Component
public class CurrencyService {


    public static String getCurrency(String value) {
        try {

            URL url = new URL("https://api.nbrb.by/exrates/rates/" + value + "?parammode=2");
            Scanner scanner = new Scanner((InputStream) url.getContent());
            StringBuilder result = new StringBuilder();

            while (scanner.hasNext()){
                result.append(scanner.nextLine());
            }

            JSONObject json = new JSONObject(result.toString());
            BigDecimal currencyRate = json.getBigDecimal("Cur_OfficialRate");
            String inputDate = json.getString("Date");

            DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            Date d = inputFormat.parse(inputDate);
            String date = formatter.format(d);

            return "Курс для " + value + " равен: " + currencyRate + ", на " + date;
        } catch (IOException | ParseException e) {
            return  null;
        }
    }
}
