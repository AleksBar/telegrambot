package TheMasccJavaJuniorTelegramBot.main.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application.properties")
@Getter

public class TelegramBotConfig {
    @Value("${bot.name}")
    public String botName;
    @Value("${bot.token}")
    public String botToken;
}
