package TheMasccJavaJuniorTelegramBot.main.config;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CommandsStore {
    Map<String, Boolean> commandsStore = new HashMap<>();
    List<String> windows;

    public CommandsStore(List<String> windows) {
        this.windows = windows;
    }

    public void init(){
        if(windows.size() > 0){
            windows.forEach(window -> {
                commandsStore.put(window, false);
            });
        }
    }

    public boolean checkState(String windows){
        if(commandsStore.containsKey(windows)){
            return commandsStore.get(windows);
        }
        return false;
    }

    public void setActiveState(String windows){
        if(commandsStore.containsKey(windows)){
            commandsStore.keySet().forEach(key -> {
                commandsStore.put(key, false);
            });
            commandsStore.put(windows, true);
        }
    }
}
