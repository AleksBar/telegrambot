package TheMasccJavaJuniorTelegramBot.main.config;

import TheMasccJavaJuniorTelegramBot.main.service.TelegramBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Component
public class BotInitializer {
    private final TelegramBot telegramBot;

    @Autowired
    public BotInitializer(TelegramBot telegramBot) {
        this.telegramBot = telegramBot;
    }

    @EventListener({ContextRefreshedEvent.class})
    public void init() throws TelegramApiException {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        CommandsStore store = new CommandsStore(telegramBot.getWindows());
        store.init();
        telegramBot.setStore(store);
        try {
            telegramBotsApi.registerBot(telegramBot);
        } catch (TelegramApiException e){
            System.out.println("Can't refresh bot");
        }
    }
}
